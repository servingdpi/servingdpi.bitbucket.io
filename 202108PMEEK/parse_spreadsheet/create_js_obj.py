#!/usr/bin/env python3
import configparser
import csv
import datetime
import numpy as np
import sys

###########################################################
# Note: Modify the config.ini as required
# usage: python3 create_js_obj.py <data_spreadsheet.csv>
###########################################################


def time_formatter(hour, timefraction):
    '''return the time fraction as a 24 hour repressentation'''
    if timefraction == 1:
        # change of day
        hour += 1
        return "%d.00.00" % hour
    else:
        mins = 24*timefraction
        if mins < 10.0:
            return "%d.0%.1f" % (hour, mins)
        else:
            return "%d.%.1f" % (hour, mins)

def find_nearest(array, value):
    '''find nearest 30min interval to current timestamp'''
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]

def xldate_as_datetime(xldate, datemode):
    # datemode: 0 for 1900-based, 1 for 1904-based
    epoch = datetime.datetime(1899, 12, 30)
    excel_date = datetime.timedelta(days=xldate + 1462 * datemode)
    return epoch + excel_date


config = configparser.ConfigParser()
config.read(u'config.ini')

# read the required csv cols from the config file
id_col = int(config['data_columns']['ID'])
lat_col = int(config['data_columns']['latitude'])
long_col = int(config['data_columns']['longitude'])
tstamp_col = int(config['data_columns']['timestamp'])

print("Using csv column %s for ID" % id_col)
print("Using csv column %s for latitude data" % lat_col)
print("Using csv column %s for longitude data" % long_col)
print("Using csv column %s for timestamp" % tstamp_col)

infile = sys.argv[1]
outfile = "XXXX.js"

tstamps = []
lats = []
longs = []
ids = []
# Broken. I think this is the issue, I need a way of getting
# the max date range, ie start with the dog that is measured frist
# end with the dog that is measured last. ATM I think I am overwrite
# each time
day_counter = []

with open(infile) as f:
    csv_reader = csv.reader(f)
    header = next(csv_reader)
    print("""First line Header %s\nSkipped! If this is really data
    add a dummy header to the file
    """ % header)
    for row in csv_reader:
        tstamps.append(row[tstamp_col])
        day_counter.append(float(row[tstamp_col]))
        lats.append(row[lat_col])
        longs.append(row[long_col])
        ids.append(row[id_col])

# should all be the same length
assert len(tstamps) == len(lats)
assert len(longs) == len(lats)
assert len(ids) == len(lats)

# sort the day counter in case these are not in order
day_counter.sort()
# calculate the number of days covered by this experiment based on t-stamp
start_day = int(str(day_counter[0]).split('.')[0])
print(start_day)
end_day = int(str(day_counter[-1]).split('.')[0]) + 1
print(end_day)

# 48 lots of 30 min in a 24 hour day, get the fraction
counter30 = [n*(1.0/48.0) for n in range(0,48)]
# test: the counters for 30 minute intervals and days up until the end of the exp 42875
counter_days = [n for n in range(0, (end_day - start_day))]

exp_range = [time_formatter(days, mins) for days in counter_days
             for mins in counter30]

start_day = float(start_day)
day_fraction = []
for days in counter_days:
    for mins in counter30:
        day_fraction.append(start_day)
        start_day+= (1.0/48.0)

# final_vals holds a time tag that we can add as a column and a time
# stamp fractional value
final_vals = [(day_label, day_fraction[n]) for n, day_label in enumerate(exp_range)]

## final_vals[:50]                                                                                                                                                             [47/3284]
##   [('0.00.0', 42403.0),
##    ('0.00.5', 42403.020833333336),
##    ('0.01.0', 42403.04166666667),
##    ('0.01.5', 42403.06250000001),

assert len(day_fraction) == len(exp_range)
assert len(day_fraction) == len(final_vals)

# now to process the data with the above tags
dog_data = {}
dog_tstamp = {}

# this is a dict dogid, then timestamp, then row data 
for i,tstamp in enumerate(tstamps):
    tstamp = float(tstamp)
    if ids[i] in dog_data:
        dog_data[ids[i]][round(tstamp, 8)] = [ids[i], lats[i],
                                              longs[i],tstamp]
    else:
        dog_data[ids[i]] = {round(tstamp, 8): [ids[i], lats[i],
                                              longs[i],tstamp]}
    if ids[i] in dog_tstamp:
        dog_tstamp[ids[i]].append(round(tstamp, 8))
    else:
        dog_tstamp[ids[i]] = [round(tstamp, 8)]

# convert to np array
for key in dog_tstamp:
    dog_tstamp[key] = np.round(dog_tstamp[key], decimals=8)
    dog_tstamp[key].sort()

#dog_tstamp['126383']
##Out[54]: 
##    array([42403.98958333, 42404.00069444, 42404.01111111, ...,
##                  42526.45      , 42526.57361111, 42527.07430556])
#dog_data['126383'][42403.98958333]
##Out[53]: ['126383', '-35.662210', '148.172200', 42403.9895833333]

# go through list of defined tstamps, find nearest one in 
# real data and add this to the dictionary.
final_dog_dict = {}
for time_tag, tstamp in final_vals:
    # find data with the closest tstamp value for each dog
    for dog in dog_tstamp:
        # an storted np array
        tstamp_options = dog_tstamp[dog]
        # from all tstamp choice in real data, which is closest
        nearest = find_nearest(tstamp_options, tstamp)
        assert nearest in dog_data[dog]
        # this will be sorted based on time_tag loop
        if dog in final_dog_dict:
            final_dog_dict[dog].append((time_tag, tstamp, dog_data[dog][nearest]))
        else:
            final_dog_dict[dog] = [(time_tag, tstamp, dog_data[dog][nearest])]

# this list holds the final row data read for writing to js
final_rows = []
tstamp_file = open('tstamp_key.csv', 'w')
tmp = open('tmp', 'w')
tmp.write("tstampMap={")
tstamp_file.write("tstamp,stampstr,counter,\n")
log = open('log_file.txt', 'w')
log.write("(time_tag,30min_timestamp,[dogid,lat,long,nearest 30min timestamp])\n")
for dog in final_dog_dict:
    counter = 0
    current_dog = []
    for observation in final_dog_dict[dog]:
        log.write(str(observation)+"\n")
        # only record every hour to reduce file size
        #if counter%2 == 0:
        # js object
        out = '{"ID":"%s","LT":%s,"LG":%s,"CTR":"%s"}' % (
            dog, observation[2][1], observation[2][2], counter)
        current_dog.append(out)
        counter += 1
        # create time stamp labels
        if len(final_rows) == 0:
            excel_tstamp = observation[1]
            dtobj = xldate_as_datetime(excel_tstamp, 0)
            date_str = dtobj.strftime("%Y-%m-%d %H:%M")
            # minus 1 to allow for ticking counter above
            time_label = "%s,%s,%s\n" % (observation[0], date_str, counter-1)
            time_label_obj = '%s:"%s",\n' % (counter-1, date_str)
            tstamp_file.write(time_label)
            tmp.write(time_label_obj)
    final_rows.append(current_dog)
tstamp_file.close()
tmp.close()
log.close()

tmp = open('tmp').read()
with open('tstamp_key.js', 'w') as t:
    t.write(tmp[:-2])
    t.write("};")

with open('final_data.js', 'w') as w:
    w.write("var sensorData=[")
    for dog_data in final_rows:
        # need to modify the first and last dogs line to close array
        w.write("["+dog_data[0]+",\n")
        for dog in dog_data[1:-1]:
            w.write(dog+",\n")
        w.write(dog_data[-1]+"],\n")
    w.write("];")

#with open('final_dog_data_addcounter.csv', 'w') as outfile:
#csv_writer = csv.writer(outfile)

