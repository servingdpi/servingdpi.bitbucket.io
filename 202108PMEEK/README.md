# 202108PMEEK  
Animations of dogs out west.  

Based heavily on 201905GBALLARD  

## HowTo
### Part 1: pre-process the spreadsheets
Use `data_parser/moree_wellgrove_narran_preprocess.ipynb` to preprocess the
xlsx (converted to csv) files from guy. I had to modify the date-time format
because he was usinga different format in these files versus the previous
project. See the notebook for details of how this works.

## Part 2: create the JS object file
There is a script `parse_spreadsheet/create_js_obj.py` that creates the JS
object from the csv file. This script uses a config file `config.ini` to set
the critical columns.

Note: on lines 182-186 you can add a if (see commented out code) and nest the
out/append call so that it only saves data every second row, this essentially
halves the size of the file, but obviously makes the data more granular.

```
if counter%2 == 0:
    # js object
    out = '{"ID":"%s","LT":%s,"LG":%s,"CTR":"%s"}' % (
         dog, observation[2][1], observation[2][2], counter)
    current_dog.append(out)
# end nesting
counter += 1
```

## The website
- copy the final_data_...js data file (created using the above script) to `static/data`
- copy the tstamp_key_.....js file to `static/data`
- update line 44 of `static/js/leaf_moree.js` with the central coordinates of
    the new lot of animals that are being tracked.
- update the `index.html` with the paths to `static/data/tstamp_key_moree.js`
    and `static/js/leaf_moree.js` and `static/data/final_data_moree.js`.
- If this is going on the server you need to rename the `index.html` to the
    name of the location so that the spark function can call this file when
    this page is requisted.

## Making the movies
I used the following command to convert the webm file to a mp4.
```
ffmpeg -i overiew2.webm -r 30 overview2.mp4
```

I didn't use it (I used a chrome app) but you can record your desktop using
```
recordmydesktop --width=1920 height=1080 --fps=10 --no-sound --delay=10
```

