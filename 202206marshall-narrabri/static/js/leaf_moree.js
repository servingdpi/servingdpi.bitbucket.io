// globals
var moving = true;
// disable this button
var playButton = d3.select("#play-button");
//playButton.attr("disabled", "disabled");
var fastButton = d3.select("#faster-button");
fastButton.attr("disabled", "disabled");
var slowButton = d3.select("#slower-button");
slowButton.attr("disabled", "disabled");
var timer;
var timerCount = 1; // 0
var lableTag;
var playSpeed = 300;
var form = document.getElementById("radios");
var formStart = 'dogIdent';
var fastestSpeed = 50;
var slowestSpeed = 2000;
var speedStep = 20;
var showStartPos = false; // true;
var showLines = true;

// set the slider max to the last value in the tstamp file
var maxCount = Object.keys(tstampMap).length - 1;
document.getElementById("moveCounter").max = maxCount;

d3.select("#moveCounter").on("input", function(d) {
    timerCount = this.value;
    update();
});

// these are just for the innial dummay line array
var lineArray1 = [
    [-35.282820, 148.576500], [-35.900560, 148.308180]];

var lineArray2 =
    [[-35.526700, 148.393500], [-35.649120, 148.186150]];

var polylineArray = [
    L.polyline(lineArray1, {color: 'red', opacity: 0}),
    L.polyline(lineArray2, {color: 'green', opacity: 0}),
];
// var for holding lines
var polylines;
// setup the leaflet map -25.2819922,148.8072971 -30.0241453,144.4451336
var map = L.map('map').setView([-30.2624697,149.9886606], 10);
mapLink = '<a href="http://openstreetmap.org">OpenStreetMap</a> | &copy;<a href="http://www.esri.com/">Esri</a>';
        // for map start 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    L.tileLayer(
            'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
            attribution: '&copy; ' + mapLink + ' Contributors',
            maxZoom: 17,
    }).addTo(map);

// satalite map toggle
var sataliteToggle = L.easyButton({
    states: [
        {
        stateName: 'add-map',
        icon: '<ion-icon name="map"></ion-icon>',
        title: 'Show map view',
        onClick: function(control) {
            L.tileLayer(
                'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    attribution: '&copy; ' + mapLink + ' Contributors',
                    maxZoom: 18,
                }).addTo(map);
            control.state('add-sat');
        }},
	{
        stateName: 'add-sat',
        icon: '<ion-icon name="globe"></ion-icon>',
        title: 'Show satalite view',
        onClick: function(control) {
            L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
                attribution: '&copy; '+mapLink+ ' Contributors',
                maxZoom: 18,
            }).addTo(map);
            control.state('add-map');
        }},
    ],
});
sataliteToggle.addTo(map);

/*
var toggleStart = L.easyButton({
    states: [{
        stateName: 'add-markers',
        icon: '<ion-icon name="locate"></ion-icon>',
        title: 'Remove starting markers',
        onClick: function(control) {
            console.log('hide button push');
            showStartPos = false;
            update();
            control.state('remove-markers');
        },
    }, {
        icon: '<ion-icon name="locate"></ion-icon>',
        stateName: 'remove-markers',
        onClick: function(control) {
            console.log('show button push');
            showStartPos = true;
            update();
            control.state('add-markers');
        },
        title: 'Add starting markers',
    }],
});
toggleStart.addTo(map);
*/
// toggle tails
var toggleTail = L.easyButton({
    states: [{
        stateName: 'remove-lines',
        icon: '<ion-icon name="analytics"></ion-icon>',
        title: 'Remove lines',
        onClick: function(control) {
            showLines = false;
            update();
            control.state('add-lines');
        },
    }, {
        icon: '<ion-icon name="analytics"></ion-icon>',
        stateName: 'add-lines',
        onClick: function(control) {
            showLines = true;
            update();
            control.state('remove-lines');
        },
        title: 'Add lines',
    }],
});
toggleTail.addTo(map);

// required for map redraw
map._initPathRoot();

var svg = d3.select("#map").select("svg");
// gstart is ontop as it is last listed here
g = svg.append("g");
gStart = svg.append("g");

document.getElementById("day").innerHTML = tstampMap[0];
// read the data would go here in fish script
console.log('reading file');
var allData = {};
allCords = {}; // holds lat, long for polylines, per day
for (var i=0; i < sensorData.length; i++) {
    for (var y=0; y < sensorData[i].length; y++) {
        var lt = sensorData[i][y].LT;
        var lg = sensorData[i][y].LG;
        // fix switched cords for some dogs, ahhh
        if (lt > 0) {
            // sitch them
            tp = lt;
            lt = lg;
            lg = tp;
        }
        var tempLatLng = new L.LatLng(
            sensorData[i][y].LT,
            sensorData[i][y].LG);
        var tempDogID = sensorData[i][y].ID;
        // var tstamp = sensorData[i][y].tstamp;
        var k = sensorData[i][y].CTR;
        if (allData.hasOwnProperty(k)) {
            allData[k].push({"DogID": tempDogID, "LatLng": tempLatLng});
        } else {
            allData[k] = [{"DogID": tempDogID, "LatLng": tempLatLng}];
        }
        // coll cords for line properties
        if (allCords.hasOwnProperty(k)) {
            allCords[k].push([lt, lg, tempDogID]);
        } else {
            allCords[k] = [[lt, lg, tempDogID]];
        }
    }
}
// use a number passed to the color function, set these to index spec cols
colorByID = {};
for (var i = 0; i < Object.keys(allData["0"]).length; i++) {
    colorByID[allData["0"][i].DogID] = i;
}
// http://bl.ocks.org/aaizemberg/78bd3dade9593896a59d
var colors = d3.scale.category10();
var colors = (id) => {
	if (id === 0) {
		return '#ff00ff' //# #eb8034
	}
	if (id === 1) {
		return '#ff0000' 
	}
	if (id === 2) {
		return '#0000ff'
	}
	if (id === 3) {
		return '#00ffff'
	}
  if (id === 4) {
		return '#00ff00'
	}
  if (id === 5) {
		return '#ffff00'
	}
  if (id === 6) {
		return '#7f007f'
	}
  if (id === 7) {
		return '#050539'
	}
  if (id === 8) {
		return '#c3ff12' // #4c4c4c
	}
  if (id === 9) {
		return '#eb8034' //'#085c5c'
	}
  if (id === 10) {
		return '#0c5f0c'
	}
  if (id === 11) {
		return '#9b9704'
	}
  if (id === 12) {
		return '#7f7f7f'
	}
  if (id === 13) {
		return '#ffffff'
	}
  if (id === 14) {
		return '#ff006c'
	}
  if (id === 15) {
		return '#eb8034'
	}
if (id === 16) {
		return '#eb8034'
	}
if (id === 17) {
		return '#eb8034'
	}
if (id === 18) {
		return '#eb8034'
	}
if (id === 19) {
		return '#eb8034'
	}
if (id === 20) {
		return '#eb8034'
	}
if (id === 21) {
		return '#eb8034'
	}
if (id === 22) {
		return '#eb8034'
	}
if (id === 23) {
		return '#eb8034'
	}
if (id === 24) {
		return '#eb8034'
	}
if (id === 25) {
		return '#eb8034'
	}
if (id === 26) {
		return '#eb8034'
	}
if (id === 27) {
		return '#eb8034'
	}
if (id === 28) {
		return '#eb8034'
	}
}
// .domain([0, Object.keys(colorByID).length]); // 29 was orig value

// blue colors(0) male, red female, colors(6) is actually pink..
colorBySex = {'60495': 0,
	'86569': 2};
    
function iterationCopy(src) {
    var target = {};
    for (var prop in src) {
        if (src.hasOwnProperty(prop)) {
            target[prop] = src[prop];
        }
    }
    return target;
}
// inially set the color obj to by id, this will be updated
colorsObj = iterationCopy(colorByID);

d3.select("#radios").on("input", function(d) {
    for (var i=0; i<form.length; i++) {
        if (form[i].checked) {
            formStart = form[i].id;
        }
    }
    if (formStart == 'dogIdent') {
        colorsObj = iterationCopy(colorByID);
    } else {
        colorsObj = iterationCopy(colorBySex);
    }
});

// starting marker for dogs, hollow
var featureStart = gStart.selectAll(".startCircle")
    .data(allData["0"])
    .enter()
    .append("circle")
    .attr("class", "startCircle")
    .style("fill", "#FFFFFF")
    .style("stroke", function(d, i) {
        return colors(colorsObj[d.DogID]);
    })
    .style("stroke-width", 1)
    .style("opacity", 0)
    .attr("r", 4);
// the actual dot after it starts moving away from start
var feature = g.selectAll(".circle")
    .data(allData["0"])
    .enter()
    .append("circle")
    .attr("class", "circle")
    // .style("fill-opacity", 0) makes just worm, no dot
    .style("fill", function(d, i) {
        return colors(colorsObj[d.DogID]);
     })
    .attr("r", 14);
// text label for starting pos
var startText = gStart.selectAll("text")
    .data(allData["0"])
    .enter()
    .append("text")
    .text(function(d) {
        return d.DogID;
    })
    .attr("class", "dogids")
    .attr("y", function(d) {
        return -5;
    });

var polylines = L.layerGroup(polylineArray);
    polylines.addTo(map);
map.on("viewreset", update);
update();

function step() {
    // update the data object
    if (moving == true) {
        if (timerCount >= maxCount) {
            // really need to break, this is inf loop
            moving = false;
        } else {
            lableTag = tstampMap[timerCount];
            // line about updating timer ticker in document
            document.getElementById("day").innerHTML = lableTag;
            try {
                // added enter, not sure if I should have, length changes?
                feature.data(allData[timerCount]).enter();
            update();
            } catch (err) {
                // no data for this counter
                // console.log('no data for ' + timerCount);
            }
            timerCount++;
            // update the slider
            document.getElementById("moveCounter").value = timerCount;
        }
    }
}
// start the animation on page load
window.onload = function() {
	    timer = setInterval(step, playSpeed);
        //#button.text("Pause");
		playButton.text("Pause");
        slowButton.attr("disabled", null);
        fastButton.attr("disabled", null);
};

playButton.on("click", function() {
    var button = d3.select(this);
    if (button.text() == "Pause") {
        moving = false;
        clearInterval(timer);
        button.text("Play");
        } else {
        moving = true;
        timer = setInterval(step, playSpeed);
        button.text("Pause");
        slowButton.attr("disabled", null);
        fastButton.attr("disabled", null);
        }
});
fastButton.on("click", function() {
    if (playSpeed > fastestSpeed) {
    playSpeed = playSpeed - speedStep;
    clearInterval(timer);
    timer = setInterval(step, playSpeed);
    } else {
        fastButton.attr("disabled", "disabled");
    }
    if (playSpeed < slowestSpeed) {
        slowButton.attr("disabled", null);
    }
});
slowButton.on("click", function() {
    if (playSpeed < slowestSpeed) {
    playSpeed = playSpeed + speedStep;
    clearInterval(timer);
    timer = setInterval(step, playSpeed);
    } else {
        slowButton.attr("disabled", "disabled");
    }
    if (playSpeed > fastestSpeed) {
        fastButton.attr("disabled", null);
    }
});

function update() {
    // draw the paths, opacity set to zero on dots
    if (timerCount > 0) {
        try {
            map.removeLayer(polylines);
        } catch (err) {
            console.log('no layer group');
        }
        polylines = L.layerGroup(makePolyline(timerCount));
        polylines.addTo(map);
    }
    feature
        .style("fill", function(d, i) {
            return colors(colorsObj[d.DogID]);
            //    }
        })

        .style("opacity", function(d) {
            if (showLines == true) {
                // hide dots
                return 0;
            } else {
                // show dots
                return 1;
            }
        })
        .attr("transform", function(d) {
            return "translate(" +
                map.latLngToLayerPoint(d.LatLng).x + "," +
                map.latLngToLayerPoint(d.LatLng).y + ")";
        }
    )
		.attr("r", 6) // sets the dot size with NO tail
		.attr("stroke", "black");
    // this needed to keep green dots on map after zoom
    featureStart
        .style("fill", "#FFFFFF") //"#b6db76"
        .style("stroke", function(d, i) {
            return colors(colorsObj[d.DogID]);
        })
        .attr("transform", function(d) {
            return "translate(" +
                map.latLngToLayerPoint(d.LatLng).x + "," +
                map.latLngToLayerPoint(d.LatLng).y + ")";
        }
        )
        .style("opacity", function(d) {
            if (showStartPos == true) {
                return 1;
            } else {
                return 0;
            }
        });
    startText
        .attr("transform", function(d) {
            return "translate(" +
                map.latLngToLayerPoint(d.LatLng).x + "," +
                map.latLngToLayerPoint(d.LatLng).y + ")";
        })
        .style("opacity", function(d) {
            if (showStartPos == true) {
                return 1;
            } else {
                return 0;
            }
        })

    // uncomment for labels on moving dots
     startText.attr("transform", function(d) {
        return "translate(" +
                map.latLngToLayerPoint(d.LatLng).x + "," +
                map.latLngToLayerPoint(d.LatLng).y + ")";
     });
}

function makePolyline(day) {
    // this value could be interactive, chose length of tail
    var updatesCovered = 15;
    var myPolylineArray = [];
    if (day > updatesCovered) {
        // made this <=day, thus should include current point
        for (var x=day-updatesCovered; x <= day; x++) {
            try {
            for (var y=0; y < allCords[x].length; y++) {
                try {
                    myPolylineArray[y].push(allCords[x][y]);
                } catch (err) {
                    myPolylineArray.push([]);
                    myPolylineArray[y].push(allCords[x][y]);
                }
            }
            } catch (err) {
                // no entry for this day
            }
        }
    } else {
        for (var x=0; x<=day; x++) {
            try {
            for (y=0; y < allCords[x].length; y++) {
                try {
                    myPolylineArray[y].push(allCords[x][y]);
                } catch (err) {
                    myPolylineArray.push([]);
                    myPolylineArray[y].push(allCords[x][y]);
                }
            }
            } catch (err) {
                // console.log('no line data');
            }
        }
    }
    polylineArray = [];
    // make the polyline object that will be added to a group in the layer
    for (var z=0; z<myPolylineArray.length; z++) {
        var cords = [];
        var dogid = myPolylineArray[z][0][2];
        // polylineArray.push(L.polyline(myPolylineArray[z], {color: colors(z),
        for (var q=0; q < myPolylineArray[z].length; q++) {
            // now chop of the dogid from each array
            cords.push(myPolylineArray[z][q].slice(0, 2));
        }
        cords.reverse();
        polylineArray.push(L.polyline(cords, {color: colors(colorsObj[dogid]),
			weight: 9, opacity: 1}));
    }
    // this could be interactive, pick dog to add tail
    // use return [polylineArray[2]] to return data for single dog(ie dog3)
    if (showLines == true) {
        return polylineArray;
    } else {
        return [];
    }
}
