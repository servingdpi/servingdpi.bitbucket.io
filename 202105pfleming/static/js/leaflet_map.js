var filename = 'static/data/data.json'

async function drawMap(filename) {
  // 1 month data
  // filename = 'static/data/data.json'
  // 6 month data
  //filename = 'static/data/data_sixmon.json'
  const dataset = await d3.json(filename)

  let polylineArray = []

  // setup the map
  var map = L.map('map').setView([-26.3768, 150.2], 11);
  mapLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>';
  var layer = L.tileLayer(
  'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
    attribution: '&copy; ' + mapLink + ' Contributors',
    maxZoom: 18,
  })
  map.addLayer(layer)
  // satalite map toggle
  var sataliteToggle = L.easyButton({
    states: [
	{
        stateName: 'add-map',
        icon: '<ion-icon name="map"></ion-icon>',
        title: 'Show map view',
        onClick: function(control) {
            L.tileLayer(
                'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    attribution: '&copy; ' + mapLink + ' Contributors',
                    maxZoom: 18,
                }).addTo(map);
            control.state('add-sat');
        }},
        {
        stateName: 'add-sat',
        icon: '<ion-icon name="globe"></ion-icon>',
        title: 'Show satalite view',
        onClick: function(control) {
            L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
                attribution: '&copy; '+mapLink+ ' Contributors',
                maxZoom: 18,
            }).addTo(map);
            control.state('add-map');
        }},
    ],
  });
  sataliteToggle.addTo(map);

  const addPaths = (monthIx) => {
      // ix 0 = month ix 1 = six months
    // reset the polyline array after call to add/remove data
    polylineArray = []

    // let polylines
    var popup2 = L.popup({'className': 'custom-popup'})
    var startDogName = 'ET73'
    var startPopup = "<p><b>"+startDogName+"</b>"+
          "</br>"+dataset[monthIx][startDogName].img+
          "</br>"+dataset[monthIx][startDogName].age+" year old "+
          dataset[monthIx][startDogName].sex+
          "</br>"+dataset[monthIx][startDogName].dist+" kms travelled</p>"

    for (const dog in dataset[monthIx]) {
      if (dog == startDogName) {
        var op = 1.0
        var col = dataset[monthIx][dog].color
        popup2.setLatLng(dataset[monthIx][dog].coordinates[0])
          .setContent(startPopup)
          .openOn(map)
      } else {
        var op = 0.5
        var col = "orange"
      }
      polylineArray.push(L.polyline(dataset[monthIx][dog].coordinates,
        {name: dog, color: col, opacity: op }))

    }
    var svg = d3.select(map.getPanes().overlayPane).append("svg")
    var g = svg.append("g").attr("class", "leaflet-zoom-hide")
    // remote previous
    //map.removeLayer(polylines)
    polylines = L.featureGroup(polylineArray).addTo(map)
    // click for click behaviour rather htan mouse over
    polylines.on("mouseover", e => {
      update(e["layer"]["options"].name)
    })

    function update(dogName) {
      // this will remove the current line styles
      map.removeLayer(polylines)
      var myPolylineArray = []
      for (const dog in dataset[monthIx]) {
        if (dog == dogName) {
          var op = 1.0
          // all red in this data as a placeholder
          var col = dataset[monthIx][dog].color
        } else {
          var op = 0.5
          var col = "orange"
        }
        myPolylineArray.push(L.polyline(dataset[monthIx][dog].coordinates,
          {name: dog, color: col, opacity: op}))
      }
      polylines = L.featureGroup(myPolylineArray)
      polylines.addTo(map)
      polylines.on("mouseover", function(e) {
        var dogName = e["layer"]["options"].name
        // ToDo: move this up into update, then call update to update
        var popupContent = "<p><b>"+dogName+"</b>"+
          "</br>"+dataset[monthIx][dogName].img+
          "</br>"+dataset[monthIx][dogName].age+" year old "+
          dataset[monthIx][dogName].sex+
          "</br>"+dataset[monthIx][dogName].dist+" kms travelled</p>"
        popup2.setLatLng(e.latlng)
          // add other info to popup hear
          .setContent(popupContent)
          .openOn(map)
        update(dogName)
      })
    } // end of update
  } // end of add paths
  // runs on load
  let monthIx = 0
  addPaths(monthIx)
  const button = d3.select("body")
    .append("button")
      .attr("class", "button")
      .text("Show more data")

  button.node().addEventListener("click", onClick)
  function onClick() {
    // runs on click
    map.removeLayer(polylines)
    if (monthIx == 0) {
      monthIx = 1
      button.text("Only most recent")}
    else {
      monthIx = 0
      button.text("Show more data")
    }
    addPaths(monthIx)
  }
}
drawMap('static/data/data.json')
