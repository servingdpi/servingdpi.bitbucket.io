This is a repo that links to `https://servingdpi.bitbucket.io/` for hosting pages from bitbucket. 

1. Clone this repo and add the directory you want to serve to the internet  
2. ie 201912ccarney  
3. copy a index.html and static dirs into this new directory

```
201912ccarney/index.html
201912ccarney/static/...

```
	  
4. push this to the repo at bitbucket  
5. navigate to https://servingdpi.bitbucket.io/201912ccarney/  

Make sure the directory has no special chars so the url works.  
